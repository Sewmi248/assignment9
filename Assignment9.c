#include <stdio.h>

int main()
{
    FILE *fptr;
    char sen[100];

    fptr = fopen("assignment9.txt", "w");
    fprintf(fptr, "UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(fptr);

    fptr = fopen("assignment9.txt", "r");
    fscanf(fptr, "%[^\n]", sen);
    printf("\n%s", sen);
    fclose(fptr);

    fptr = fopen("assignment9.txt", "a");
    fprintf(fptr, "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(fptr);

    return 0;
}
